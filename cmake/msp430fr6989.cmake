set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR msp430)

set(MCU "msp430fr6989")

set(MCPU_FLAGS "-mmcu=${MCU}")
set(LD_FLAGS "-T ${MCU}.ld")

include(${CMAKE_CURRENT_LIST_DIR}/gcc-msp430.cmake)

set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR msp430)

set(CMAKE_SYSROOT "/opt/ti/mspgcc")
include_directories("${CMAKE_SYSROOT}/include")
link_directories("${CMAKE_SYSROOT}/include")

set(TARGET_TRIPLET "msp430-elf")

set(OBJCOPY ${TARGET_TRIPLET}-objcopy CACHE INTERNAL "objcopy tool")
set(OBJDUMP ${TARGET_TRIPLET}-objdump CACHE INTERNAL "objdump tool")
set(OBJSIZE ${TARGET_TRIPLET}-size CACHE INTERNAL "objsize tool")
set(CMAKE_ASM_COMPILER ${TARGET_TRIPLET}-as CACHE INTERNAL "asm compiler")
set(CMAKE_C_COMPILER ${TARGET_TRIPLET}-gcc  CACHE INTERNAL "c compiler")
set(CMAKE_CXX_COMPILER ${TARGET_TRIPLET}-g++ CACHE INTERNAL "cpp compiler")
set(DEBUGGER ${TARGET_TRIPLET}-gdb CACHE INTERNAL "debugger")

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

set(CMAKE_C_FLAGS   "${MCPU_FLAGS} -fdata-sections \
                    -ffunction-sections -fno-common -fno-builtin"
                         CACHE INTERNAL "c compiler flags")
set(CMAKE_CXX_FLAGS "${MCPU_FLAGS} -fdata-sections \
                    -ffunction-sections -fno-common -fno-builtin \
                    -fno-rtti -fno-exceptions -Wno-deprecated -Wno-register"
                         CACHE INTERNAL "cxx compiler flags")
set(CMAKE_ASM_FLAGS "${MCPU_FLAGS} -x assembler-with-cpp"
                         CACHE INTERNAL "asm compiler flags")
set(CMAKE_EXE_LINKER_FLAGS "${LD_FLAGS} -Wl,--relax,--gc-sections\
                            -L ${CMAKE_SYSROOT}/include"
                         CACHE INTERNAL "exe link flags")

set(CMAKE_C_FLAGS_DEBUG "-Og -g3 -gdwarf-2 -ggdb -flto"
                         CACHE INTERNAL "c debug compiler flags")
set(CMAKE_CXX_FLAGS_DEBUG "-Og -g3 -gdwarf-2 -ggdb -flto"
                         CACHE INTERNAL "cxx debug compiler flags")
set(CMAKE_ASM_FLAGS_DEBUG "-g -g3 -gdwarf-2 -ggdb -flto"
                         CACHE INTERNAL "asm debug compiler flags")

set(CMAKE_C_FLAGS_RELEASE "-O3 -flto -DNDEBUG"
                         CACHE INTERNAL "c release compiler flags")
set(CMAKE_CXX_FLAGS_RELEASE "-O3 -flto -DNDEBUG"
                         CACHE INTERNAL "cxx release compiler flags")
set(CMAKE_ASM_FLAGS_RELEASE ""
                         CACHE INTERNAL "asm release compiler flags")


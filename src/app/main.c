#include <stdbool.h>
#include <stdint.h>

#include <msp430.h>


static void stop_watchdog(void);

int main(void)
{
    stop_watchdog();
}

static void stop_watchdog(void)
{
    WDTCTL = WDTPW | WDTHOLD;
}

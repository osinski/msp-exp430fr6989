# MSP-EXP430FR6989 Starter Project

# How to build

```
mkdir build && cd build
cmake -DCMAKE_TOOLCHAIN_FILE=../cmake/msp430fr6989.cmake -DCMAKE_BUILD_TYPE=Debug -GNinja ..
ninja
```
